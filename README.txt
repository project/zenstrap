Zenstrap is a combination of Zen and Twitter Bootstrap. 

Setup
1. Download the Twitter Bootstrap zip from http://twitter.github.com/bootstrap/assets/bootstrap.zip
2. Extract the zip and place it in the theme folder so the css, js files are in
 <theme path>\zenstrap\bootstrap
3. The theme needs jquery 1.7. You need to install the dev version of jquery_update module and change
the settings to use 1.7

Font icon and Awesome fonts integration:
If you want to use the native bootstrap fonts then you can skip steps 1 & 2.

1. Download and copy the awesome-fonts folder from http://fortawesome.github.io/Font-Awesome/ to
sites/all/libraries or any suitable folder
2. Update the settings.php with the path to awesome fonts sytle sheet e.g.

$conf['awesome_path'] = 'sites/all/libraries/font-awesome/css/font-awesome.css';

3. In the Zenstrap Settings select "Use font icons" and map the icons to menus
You are good to go!
