<?php
/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function zenstrap_form_system_theme_settings_alter(&$form, &$form_state, $form_id = NULL)  {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }
  $form['zenstrap_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Zenstrap Settings'),
    '#weight' => 0,
  );
  $form['zenstrap_settings']['zenstrap_login_modal'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use modal dialog for login'),
    '#default_value' => theme_get_setting('zenstrap_login_modal'),
    '#description'   => t('Login links will be opened as a modal dialog'),
  );

  $menus = menu_get_menus();
  $form['zenstrap_settings']['zenstrap_single_accordion'] = array(
    '#type'          => 'select',
    '#options'       => $menus,
    '#title'         => t('Select menu to show as accordion'),
    '#default_value' => theme_get_setting('zenstrap_single_accordion'),
    '#description'   => t('Ensure that these menu block is added to the required region.'),
    '#empty_option' => t('Select'),
    '#empty_value' => 0,
  );
  $def_accor = theme_get_setting('zenstrap_menu_accordion');
  if (!is_array($def_accor)) {
    $def_accor = array();
  }
  $form['zenstrap_settings']['zenstrap_menu_accordion'] = array(
    '#type'          => 'checkboxes',
    '#options'       => $menus,
    '#title'         => t('Select menus to group as accordion'),
    '#default_value' => $def_accor,
    '#description'   => t('Ensure that these menu blocks are added to the same block region.'),
  );
 
  $form['zenstrap_settings']['zenstrap_disable_sticky_tables'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Disable sticky tables'),
    '#default_value' => theme_get_setting('zenstrap_disable_sticky_tables'),
    '#description'   => t('Selecting this will not generate sticky tables'),
  );
  $form['zenstrap_settings']['zenstrap_searchbox'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show search box in navigation'),
    '#default_value' => theme_get_setting('zenstrap_searchbox'),
    '#description'   => t('Shows the search box in top navigation bar'),
  );
  $form['zenstrap_settings']['zenstrap_horizontal_forms'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use horizontal forms'),
    '#default_value' => theme_get_setting('zenstrap_horizontal_forms'),
    '#description'   => t('Forms are styled in horizontal form style. See !link', 
      array('!link' => l(t('Base styles'), 'http://twitter.github.com/bootstrap/base-css.html#forms'))),
  );
  $form['zenstrap_settings']['zenstrap_modal_forms'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Launch Modal'),
    '#default_value' => theme_get_setting('zenstrap_modal_forms'),
    '#description'   => t('Enter the one URL per line that needs must be launched in modal frame'),
  );
  global $conf;
  
  $awesome_file = isset($conf['awesome_path'])? $conf['awesome_path'] : drupal_get_path('theme', 'zenstrap') . '/bootstrap/css/bootstrap.css';
  drupal_add_css($awesome_file);
  $form['zenstrap_settings']['use_menu_icons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use font icons'),
    '#default_value' => theme_get_setting('use_menu_icons'),
  );
  $form['zenstrap_settings']['menu_icons'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu Icons'),
    // '#collapsible' => TRUE,
    '#prefix' => _zenstrap_get_awesome_display(),
    '#states' => array(
      array('visible' => array(':input[name="use_menu_icons"]' => array('checked' => TRUE),),),
    ),
  );
  $menus = menu_get_menus();
  drupal_add_js(drupal_get_path('theme', 'zenstrap') . '/js/zenstrap.awesome.js');
  drupal_add_css(drupal_get_path('theme', 'zenstrap') . '/css/zenstrap.admin.css');
  foreach ($menus as $mkey => $name) {
    $form['zenstrap_settings']['menu_icons'][$mkey] = array(
      '#type' => 'fieldset',
      '#title' => $mkey,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    
    $tree = menu_tree($mkey);
    foreach ($tree as $item) {
      _zenstrap_create_menu_icons_items($form['zenstrap_settings']['menu_icons'][$mkey], $item, 0);
    }
  }
  
}

function _zenstrap_create_menu_icons_items(&$form, $item, $depth) {
  if (!isset($item['#href'])) {
    return;
  }
  $prefix = str_repeat('-', $depth);
  $icon = theme_get_setting($item['#href']);
  $form[$item['#href']] = array(
    '#type' => 'hidden',
    '#default_value' => $icon,
  );
  if (empty($icon)) { 
    $icon = 'icon-plus';
  }
  $form[$item['#href'] . 'text'] = array(
    '#type' => 'markup',
    '#markup' => '<div>' . $prefix . l($item['#title'], $item['#href']) .
      '<span class="awesome ' . $icon . '" key = "' . $icon . '" href="' . $item['#href'] . '"></span>' . '</div>',
  );
  if (!empty($item['#below'])) {
    foreach($item['#below'] as $bitem) {
      _zenstrap_create_menu_icons_items($form, $bitem, $depth + 1);
    }
  }
}

function _zenstrap_get_awesome_display() {
  $awesome_fonts = array(
    'icon-glass',  'icon-music',  'icon-search', 'icon-envelope-alt', 'icon-heart',  'icon-star', 'icon-star-empty', 'icon-user', 'icon-film', 'icon-th-large', 'icon-th', 'icon-th-list',  'icon-ok', 'icon-remove', 'icon-zoom-in',  'icon-zoom-out', 'icon-off',  'icon-signal', 'icon-cog',  'icon-trash',  'icon-home', 'icon-file-alt', 'icon-time', 'icon-road', 'icon-download-alt', 'icon-download', 'icon-upload', 'icon-inbox',  'icon-play-circle',  'icon-repeat', 'icon-refresh',  'icon-list-alt', 'icon-lock', 'icon-flag', 'icon-headphones', 'icon-volume-off', 'icon-volume-down',  'icon-volume-up',  'icon-qrcode', 'icon-barcode',  'icon-tag',  'icon-tags', 'icon-book', 'icon-bookmark', 'icon-print',  'icon-camera', 'icon-font', 'icon-bold', 'icon-italic', 'icon-text-height',  'icon-text-width', 'icon-align-left', 'icon-align-center', 'icon-align-right',  'icon-align-justify',  'icon-list', 'icon-indent-left',  'icon-indent-right', 'icon-facetime-video', 'icon-picture',  'icon-pencil', 'icon-map-marker', 'icon-adjust', 'icon-tint', 'icon-edit', 'icon-share',  'icon-check',  'icon-move', 'icon-step-backward',  'icon-fast-backward',  'icon-backward', 'icon-play', 'icon-pause',  'icon-stop', 'icon-forward',  'icon-fast-forward', 'icon-step-forward', 'icon-eject',  'icon-chevron-left', 'icon-chevron-right',  'icon-plus-sign',  'icon-minus-sign', 'icon-remove-sign',  'icon-ok-sign',  'icon-question-sign',  'icon-info-sign',  'icon-screenshot', 'icon-remove-circle',  'icon-ok-circle',  'icon-ban-circle', 'icon-arrow-left', 'icon-arrow-right',  'icon-arrow-up', 'icon-arrow-down', 'icon-share-alt',  'icon-resize-full',  'icon-resize-small', 'icon-plus', 'icon-minus',  'icon-asterisk', 'icon-exclamation-sign', 'icon-gift', 'icon-leaf', 'icon-fire', 'icon-eye-open', 'icon-eye-close',  'icon-warning-sign', 'icon-plane',  'icon-calendar', 'icon-random', 'icon-comment',  'icon-magnet', 'icon-chevron-up', 'icon-chevron-down', 'icon-retweet',  'icon-shopping-cart',  'icon-folder-close', 'icon-folder-open',  'icon-resize-vertical',  'icon-resize-horizontal',  'icon-bar-chart',  'icon-twitter-sign', 'icon-facebook-sign',  'icon-camera-retro', 'icon-key',  'icon-cogs', 'icon-comments', 'icon-thumbs-up-alt',  'icon-thumbs-down-alt',  'icon-star-half',  'icon-heart-empty',  'icon-signout',  'icon-linkedin-sign',  'icon-pushpin',  'icon-external-link',  'icon-signin', 'icon-trophy', 'icon-github-sign',  'icon-upload-alt', 'icon-lemon',  'icon-phone',  'icon-check-empty',  'icon-bookmark-empty', 'icon-phone-sign', 'icon-twitter',  'icon-facebook', 'icon-github', 'icon-unlock', 'icon-credit-card',  'icon-rss',  'icon-hdd',  'icon-bullhorn', 'icon-bell', 'icon-certificate',  'icon-hand-right', 'icon-hand-left',  'icon-hand-up',  'icon-hand-down',  'icon-circle-arrow-left',  'icon-circle-arrow-right', 'icon-circle-arrow-up',  'icon-circle-arrow-down',  'icon-globe',  'icon-wrench', 'icon-tasks',  'icon-filter', 'icon-briefcase',  'icon-fullscreen', 'icon-group',  'icon-link', 'icon-cloud',  'icon-beaker', 'icon-cut',  'icon-copy', 'icon-paper-clip', 'icon-save', 'icon-sign-blank', 'icon-reorder',  'icon-list-ul',  'icon-list-ol',  'icon-strikethrough',  'icon-underline',  'icon-table',  'icon-magic',  'icon-truck',  'icon-pinterest',  'icon-pinterest-sign', 'icon-google-plus-sign', 'icon-google-plus',  'icon-money',  'icon-caret-down', 'icon-caret-up', 'icon-caret-left', 'icon-caret-right',  'icon-columns',  'icon-sort', 'icon-sort-down',  'icon-sort-up',  'icon-envelope', 'icon-linkedin', 'icon-undo', 'icon-legal',  'icon-dashboard',  'icon-comment-alt',  'icon-comments-alt', 'icon-bolt', 'icon-sitemap',  'icon-umbrella', 'icon-paste',  'icon-lightbulb',  'icon-exchange', 'icon-cloud-download', 'icon-cloud-upload', 'icon-user-md',  'icon-stethoscope',  'icon-suitcase', 'icon-bell-alt', 'icon-coffee', 'icon-food', 'icon-file-text-alt',  'icon-building', 'icon-hospital', 'icon-ambulance',  'icon-medkit', 'icon-fighter-jet',  'icon-beer', 'icon-h-sign', 'icon-plus-sign-alt',  'icon-double-angle-left',  'icon-double-angle-right', 'icon-double-angle-up',  'icon-double-angle-down',  'icon-angle-left', 'icon-angle-right',  'icon-angle-up', 'icon-angle-down', 'icon-desktop',  'icon-laptop', 'icon-tablet', 'icon-mobile-phone', 'icon-circle-blank', 'icon-quote-left', 'icon-quote-right',  'icon-spinner',  'icon-circle', 'icon-reply',  'icon-github-alt', 'icon-folder-close-alt', 'icon-folder-open-alt',  'icon-expand-alt', 'icon-collapse-alt', 'icon-smile',  'icon-frown',  'icon-meh',  'icon-gamepad',  'icon-keyboard', 'icon-flag-alt', 'icon-flag-checkered', 'icon-terminal', 'icon-code', 'icon-reply-all',  'icon-mail-reply-all', 'icon-star-half-empty',  'icon-location-arrow', 'icon-crop', 'icon-code-fork',  'icon-unlink', 'icon-question', 'icon-info', 'icon-exclamation',  'icon-superscript',  'icon-subscript',  'icon-eraser', 'icon-puzzle-piece', 'icon-microphone', 'icon-microphone-off', 'icon-shield', 'icon-calendar-empty', 'icon-fire-extinguisher',  'icon-rocket', 'icon-maxcdn', 'icon-chevron-sign-left',  'icon-chevron-sign-right', 'icon-chevron-sign-up',  'icon-chevron-sign-down',  'icon-html5',  'icon-css3', 'icon-anchor', 'icon-unlock-alt', 'icon-bullseye', 'icon-ellipsis-horizontal',  'icon-ellipsis-vertical',  'icon-rss-sign', 'icon-play-sign',  'icon-ticket', 'icon-minus-sign-alt', 'icon-check-minus',  'icon-level-up', 'icon-level-down', 'icon-check-sign', 'icon-edit-sign',  'icon-external-link-sign', 'icon-share-sign', 'icon-compass',  'icon-collapse', 'icon-collapse-top', 'icon-expand', 'icon-eur',  'icon-gbp',  'icon-usd',  'icon-inr',  'icon-jpy',  'icon-cny',  'icon-krw',  'icon-btc',  'icon-file', 'icon-file-text',  'icon-sort-by-alphabet', 'icon-sort-by-alphabet-alt', 'icon-sort-by-attributes', 'icon-sort-by-attributes-alt', 'icon-sort-by-order',  'icon-sort-by-order-alt',  'icon-thumbs-up',  'icon-thumbs-down',  'icon-youtube-sign', 'icon-youtube',  'icon-xing', 'icon-xing-sign',  'icon-youtube-play', 'icon-dropbox',  'icon-stackexchange',  'icon-instagram',  'icon-flickr', 'icon-adn',  'icon-bitbucket',  'icon-bitbucket-sign', 'icon-tumblr', 'icon-tumblr-sign',  'icon-long-arrow-down',  'icon-long-arrow-up',  'icon-long-arrow-left',  'icon-long-arrow-right', 'icon-apple',  'icon-windows',  'icon-android',  'icon-linux',  'icon-dribbble', 'icon-skype',  'icon-foursquare', 'icon-trello', 'icon-female', 'icon-male', 'icon-gittip', 'icon-sun',  'icon-moon', 'icon-archive',  'icon-bug',  'icon-vk', 'icon-weibo', 'icon-renren',
  );
  $output = '<div id="awesome_fonts"><a class="cancel"><i class="icon-remove"></i>' . t('Cancel') . '</a>';
  $output .= '<a class="trash"><i class="icon-trash"></i>' . t('Delete') . '</a>';
  foreach ($awesome_fonts as $class) {
    $output .= "<div><i class='$class select-icon icon-2x' key='$class'></i>$class</div>";
  }

  $output .= '</div><div style="clear:both"></div>';
  return $output;
}
